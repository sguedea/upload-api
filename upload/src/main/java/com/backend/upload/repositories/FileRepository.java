package com.backend.upload.repositories;

import com.backend.upload.models.File;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends JpaRepository<File, String> {
    
}

// now we can use save(), findById(), findAll() 
