package com.backend.upload.services;

import java.io.IOException;
import java.util.stream.Stream;

import com.backend.upload.models.File;
import com.backend.upload.repositories.FileRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileService {

    @Autowired
    private FileRepository fileRepo;

    public File store(MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        File f = new File(fileName, file.getContentType(), file.getBytes());

        return fileRepo.save(f);
    }

    public File getFile(String id) {
        return fileRepo.findById(id).get();
    }

    public Stream<File> getAllFiles() {
        return fileRepo.findAll().stream();
    }
    
}
